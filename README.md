# Load Balancer Module

This module is capable to generate an external or internal faced application load balancer

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `internal` - internal or external faced load balancer (default false)
- `security_groups` - security groups identifiers
- `subnets` - subnet identifiers
- `vpc_id` - vpc identifier (only for external faced)
- `zone_id` - zone identifier (only for external faced)
- `healthcheck_path` - healthcheck path (only for external faced, default /healthcheck)
- `certificate_arm` - certificate arn (only for external faced)
- `domain` - base domain (only for external faced)

Usage
-----

```hcl
module "load_balancer" {
  source            = "git::https://gitlab.com/mesabg-tfmodules/load_balancer.git"

  environment       = "environment"
  name = {
    slug            = "slugname"
    full            = "Full Name"
  }

  internal          = false
  type              = "application"
  security_groups   = ["sg-xxxxxx", "sg-cccccc"]
  subnets           = ["subnet-xxxxxx", "subnet-cccccc"]
  vpc_id            = "vpc_id"
  zone_id           = "identifier"
  healthcheck_path  = "/healthcheck"
  certificate_arn   = "arn::some",
  domain            = "foo.bar.com"
}
```

```hcl
module "load_balancer" {
  source            = "../../modules/load_balancer"

  environment       = "environment"
  name = {
    slug            = "slugname"
    full            = "Full Name"
  }

  internal          = true
  security_groups   = ["sg-xxxxxx", "sg-cccccc"]
  subnets           = ["subnet-xxxxxx", "subnet-cccccc"]
}
```

Outputs
=======

 - `load_balancer_arn` - Created ALB resource ARN


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
