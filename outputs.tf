output "load_balancer_arn" {
  value       = concat(aws_lb.lb_application.*.arn, values({ for application in local.nlbs:  application.name => aws_lb.lb_network[application.name].arn }))
  description = "Created Application Load Balancer ARNs"
}

output "dns_name" {
  value       = concat(aws_lb.lb_application.*.dns_name, values({ for application in local.nlbs:  application.name => aws_lb.lb_network[application.name].dns_name }))
  description = "DNS Names created"
}

output "zone_id" {
  value       = concat(aws_lb.lb_application.*.zone_id, values({ for application in local.nlbs:  application.name => aws_lb.lb_network[application.name].zone_id }))
  description = "Zone identifiers created"
}
