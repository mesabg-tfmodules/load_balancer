module "nlb_ip_locator" {
  depends_on  = [aws_lb.lb_network]
  source      = "git::https://gitlab.com/mesabg-tfmodules/load_balancer_ip_locator.git?ref=v1.0.5"
  for_each    = { for application in local.nlbs:  application.name => application }

  name        = aws_lb.lb_network[each.value.name].name
  type        = "network"
  vpc_id      = var.vpc_id
}

#resource "aws_security_group_rule" "security_group_rule" {
#  depends_on        = [aws_lb.lb_network]
#  for_each          = toset(var.security_groups)
#  security_group_id = each.value
#  type              = "ingress"
#  from_port         = 0
#  to_port           = 65535
#  protocol          = "tcp"
#  description       = "NLB Traffic Rule"
#  cidr_blocks       = values({ for ip in flatten(values({ for application in local.nlbs:  application.name => module.nlb_ip_locator[application.name].private_ips })): ip => "${ip}/32" })
#}
