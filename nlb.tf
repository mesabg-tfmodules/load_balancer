# Network Load Balancer

resource "aws_lb" "lb_network" {
  for_each            = { for application in local.nlbs:  application.name => application }
  name                = "${each.value.name}-${var.environment}"
  internal            = false
  load_balancer_type  = "network"
  subnets             = var.subnets

  tags = {
    Name              = "${each.value.name}-${var.environment}"
    Environment       = var.environment
  }
}

resource "aws_lb_target_group" "lb_target_group_network" {
  for_each      = { for application in local.nlbs:  application.name => application }
  name          = "${each.value.name}-${var.environment}"
  port          = 80
  protocol      = "TCP"
  vpc_id        = var.vpc_id

  health_check {
    enabled     = true
    path        = each.value.healthcheck_path
    port        = "traffic-port"
    protocol    = "HTTP"
  }

  tags = {
    Name        = "${each.value.name}-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_lb_listener" "lb_listener_tcp_network" {
  for_each            = { for application in local.nlbs:  application.name => application }
  load_balancer_arn   = aws_lb.lb_network[each.value.name].arn
  port                = 80
  protocol            = "TCP"

  default_action {
    type              = "forward"
    target_group_arn  = aws_lb_target_group.lb_target_group_network[each.value.name].arn
  }
}

resource "aws_lb_listener" "lb_listener_tls_network" {
  for_each            = { for application in local.nlbs:  application.name => application }
  load_balancer_arn   = aws_lb.lb_network[each.value.name].arn
  port                = 443
  protocol            = "TLS"
  ssl_policy          = "ELBSecurityPolicy-2016-08"
  certificate_arn     = var.certificate_arn

  default_action {
    type              = "forward"
    target_group_arn  = aws_lb_target_group.lb_target_group_network[each.value.name].arn
  }
}
