terraform {
  required_version = ">= 0.13.2"
}

locals {
  albs  = [for i, z in { for application in var.applications:  application.name => application }: z if z.type == "application"]
  nlbs  = [for i, z in { for application in var.applications:  application.name => application }: z if z.type == "network"]
}
