# Application Load Balancer

resource "aws_lb" "lb_application" {
  name                = var.name
  internal            = false
  load_balancer_type  = "application"
  security_groups     = var.security_groups
  subnets             = var.subnets

  tags = {
    Name              = var.name
    Environment       = var.environment
  }
}

resource "aws_lb_listener" "lb_listener_http_application" {
  load_balancer_arn = aws_lb.lb_application.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port          = 443
      protocol      = "HTTPS"
      status_code   = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "lb_listener_https_application" {
  load_balancer_arn = aws_lb.lb_application.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.certificate_arn

  default_action {
    type            = "fixed-response"

    fixed_response {
      status_code   = 200
      content_type  = "text/html"
      message_body  = file("${path.module}/maintenance.html")
    }
  }
}

resource "aws_lb_target_group" "lb_target_group_application" {
  for_each      = { for application in local.albs:  application.name => application }
  name          = "${each.value.name}-${var.environment}"
  port          = 80
  protocol      = "HTTP"
  vpc_id        = var.vpc_id

  health_check {
    enabled     = true
    path        = each.value.healthcheck_path
    port        = "traffic-port"
    protocol    = "HTTP"
    matcher     = "200"
  }

  tags = {
    Name        = "${each.value.name}-${var.environment}"
    Environment = var.environment
  }
}

resource "aws_lb_listener_rule" "lb_listener_rule_https" {
  for_each            = { for application in local.albs:  application.name => application }
  listener_arn        = aws_lb_listener.lb_listener_https_application.arn

  condition {
    host_header {
      values          = [ each.value.domain ]
    }
  }

  condition {
    path_pattern {
      values          = [ each.value.path ]
    }
  }

  action {
    type              = "forward"
    target_group_arn  = aws_lb_target_group.lb_target_group_application[each.value.name].arn
  }
}
