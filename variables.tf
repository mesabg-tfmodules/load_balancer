variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "General load balancer name"
}

variable "security_groups" {
  type        = list(string)
  description = "Security Groups Identifiers"
  default     = []
}

variable "subnets" {
  type        = list(string)
  description = "Subnets Identifiers"
}

variable "vpc_id" {
  type        = string
  description = "VPC identifier. Only required for external faced load balancer"
  default     = null
}

variable "zone_id" {
  type        = string
  description = "Route53 Zone. Only required for external faced load balancer"
  default     = null
}

variable "certificate_arn" {
  type        = string
  description = "Certificate ARN. Only required for external faced load balancer"
  default     = null
}

variable "applications" {
  type        = list(object({
    name              = string
    type              = string
    healthcheck_path  = string
    path              = string
    domain            = string
  }))
  default     = []
  description = "Applications descriptors"
}
