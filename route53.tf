resource "aws_route53_record" "route53_record_application" {
  for_each                  = toset(distinct(values({ for application in local.albs:  application.name => application.domain })))
  zone_id                   = var.zone_id
  name                      = each.value
  type                      = "A"

  alias {
    name                    = aws_lb.lb_application.dns_name
    zone_id                 = aws_lb.lb_application.zone_id
    evaluate_target_health  = true
  }
}

resource "aws_route53_record" "route53_record_network" {
  for_each                  = { for application in local.nlbs:  application.name => application }
  zone_id                   = var.zone_id
  name                      = each.value.domain
  type                      = "A"

  alias {
    name                    = aws_lb.lb_network[each.value.name].dns_name
    zone_id                 = aws_lb.lb_network[each.value.name].zone_id
    evaluate_target_health  = true
  }
}
